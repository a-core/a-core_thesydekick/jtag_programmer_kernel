# OBSOLETE!
This module has been replaced by the more generic [jtag_kernel] and will be removed soon.

[jtag_kernel]: https://gitlab.com/a-core/a-core_thesydekick/jtag_kernel/-/tree/master

# JTAG Programmer Kernel
This module contains the method `gen_prog_waveform()` for generating an A-Core JTAG programming waveform from an input array of address-data pairs. The implementation is written in C for performance reasons.

## Installation
This module can be compiled and installed on your system with:
```shell
$ python3 setup.py install --user
```

After installation, this module can be imported in python just like any other module:

```python
import jtag_programmer_kernel
```
