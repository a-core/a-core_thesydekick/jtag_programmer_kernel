from setuptools import setup, Extension

def main():
    setup(
        name = "jtag_programmer_kernel",
        version = "0.1.0",
        description = "Compute kernel for generating A-Core JTAG programming waveforms.",
        author = "Verneri Hirvonen",
        author_email = "verneri.hirvonen@aalto.fi",
        ext_modules = [
            Extension(
                "jtag_programmer_kernel",
                sources = ["main.c"],
                extra_compile_args = ["-std=c99", "-Wall", "-Wextra", "-march=native", "-O2"]
            )
        ]
    )

if __name__ == "__main__":
    main()
