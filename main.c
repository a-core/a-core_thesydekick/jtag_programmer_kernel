// SPDX-License-Identifier: Apache-2.0

#include <Python.h>
#include <numpy/arrayobject.h>

/* Global Variables */
// There's quite a lot of helper functions used to generate the waveform.
// The actual array modifications happen at the leaf functions, which
// either require passing the array pointer down the whole call stack,
// or alternatively using a global variables to store the same information.
// These should be initialized to sensible values before using any of the
// helper functions that operate on them.
static int OUTPUT_INDEX;
static int OLD_TDI;
static int TRSTn;
static PyArrayObject* OUTPUT_ARRAY;

static void jtag_cycle_tms(int tms) {
    *((uint8_t*)PyArray_GETPTR2(OUTPUT_ARRAY, OUTPUT_INDEX, 0)) = 0;        // TCK
    *((uint8_t*)PyArray_GETPTR2(OUTPUT_ARRAY, OUTPUT_INDEX, 1)) = tms;      // TMS
    *((uint8_t*)PyArray_GETPTR2(OUTPUT_ARRAY, OUTPUT_INDEX, 2)) = OLD_TDI;  // TDI
    *((uint8_t*)PyArray_GETPTR2(OUTPUT_ARRAY, OUTPUT_INDEX, 3)) = TRSTn;    // TRSTn
    OUTPUT_INDEX++;

    *((uint8_t*)PyArray_GETPTR2(OUTPUT_ARRAY, OUTPUT_INDEX, 0)) = 1;        // TCK
    *((uint8_t*)PyArray_GETPTR2(OUTPUT_ARRAY, OUTPUT_INDEX, 1)) = tms;      // TMS
    *((uint8_t*)PyArray_GETPTR2(OUTPUT_ARRAY, OUTPUT_INDEX, 2)) = OLD_TDI;  // TDI
    *((uint8_t*)PyArray_GETPTR2(OUTPUT_ARRAY, OUTPUT_INDEX, 3)) = TRSTn;    // TRSTn
    OUTPUT_INDEX++;
}

static void jtag_cycle_tms_tdi(int tms, int tdi) {
    OLD_TDI = tdi;
    jtag_cycle_tms(tms);
}

static void idle_to_drshift(void) {
    jtag_cycle_tms(1);   // state 12 = Run-Test/Idle
    jtag_cycle_tms(0);   // state 7 = Select-DR-Scan
    jtag_cycle_tms(0);   // state 6 = Capture-DR
}

static void idle_to_irshift(void) {
    jtag_cycle_tms(1);   // state 12 = Run-Test/Idle
    jtag_cycle_tms(1);   // state 7 = Select-DR-Scan
    jtag_cycle_tms(0);   // state 4 = Select-IR-Scan
    jtag_cycle_tms(0);   // state 14 = Capture-IR
}

static void drshift_to_idle(void) {
    jtag_cycle_tms(1);   // state 1 = Exit1-DR
    jtag_cycle_tms(0);   // state 5 = Update-DR
}

static void shift(int tdi, size_t len_tdi) {
    for (size_t i = 0; i < len_tdi-1; i++) {
        jtag_cycle_tms_tdi(0, tdi & 1);
        tdi >>= 1;
    }
    jtag_cycle_tms_tdi(1, tdi & 1);
}

static void irshift_to_idle(void) {
    jtag_cycle_tms(1);   // state 9 = Exit1-IR
    jtag_cycle_tms(0);   // state 13 = Update-IR
}

static void shift_instruction(int ir, size_t len_ir) {
    idle_to_irshift();   // 4 cycles
    shift(ir, len_ir);   // `len_tdi` cycles
    irshift_to_idle();   // 2 cycles
}

static void shift_data(int tdi, size_t len_tdi) {
    idle_to_drshift();   // 3 cycles
    shift(tdi, len_tdi); // `len_tdi` cycles
    drshift_to_idle();   // 2 cycles
}

static void shift_to_chain(int ir, size_t len_ir, int tdi, size_t len_tdi) {
    shift_instruction(ir, len_ir);  // `len_ir` + 6 cycles
    shift_data(tdi, len_tdi);       // `len_tdi` + 5 cycles
}

struct jtag_config {
    int len_ir;
    int ir_addr;
    int ir_data;
    int ir_wen;
    int len_addr;
    int len_data;
    int len_wen;
};

static void populate_array(const PyArrayObject* prog_data, PyArrayObject* dest, struct jtag_config config) {
    // init global state
    OUTPUT_INDEX = 0;
    OLD_TDI = 1;
    OUTPUT_ARRAY = dest;

    // shift programming data
    // TODO: In case of performance issues
    //       * Unroll this loop
    //       * Use `#pragma omp parallel for` on the outermost loop
    int n_bytes = PyArray_DIM(prog_data,0);
    for (int i = 0; i < n_bytes; i++) {
        uint32_t addr = *((uint32_t*)PyArray_GETPTR2(prog_data, i, 0));
        uint8_t  data = *((uint32_t*)PyArray_GETPTR2(prog_data, i, 1));
        // printf("addr=%d, data=%d\n", addr, data);

        // shift in address and data
        shift_to_chain(config.ir_addr, config.len_ir, addr, config.len_addr);
        shift_to_chain(config.ir_data, config.len_ir, data, config.len_data);

        // strobe write_en
        shift_to_chain(config.ir_wen, config.len_ir, 1, config.len_wen);
        shift_to_chain(config.ir_wen, config.len_ir, 0, config.len_wen);
    }
}

// Compute the required number of rows in the output array
static unsigned int count_jtag_steps(int n_bytes, struct jtag_config config) {
    const int steps_jtag_cycle = 2;                                     // timesteps per TCK cycle
    int cycles_shift_addr = config.len_ir + config.len_addr + 11;       // len_ir + len_addr + 11
    int cycles_shift_data = config.len_ir + config.len_data + 11;       // len_ir + len_data + 11
    int cycles_strobe_wen = (config.len_ir + config.len_wen+11) * 2;    // len_ir + len_wen + 11 (twice)
    return n_bytes*steps_jtag_cycle*(cycles_shift_addr+cycles_shift_data+cycles_strobe_wen);
}

void print_config(struct jtag_config config) {
    printf("struct jtag_config {\n");
    printf("  len_ir = %d,\n", config.len_ir);
    printf("  ir_addr = %d,\n", config.ir_addr);
    printf("  ir_data = %d,\n", config.ir_data);
    printf("  ir_wen = %d,\n", config.ir_wen);
    printf("  len_addr = %d,\n", config.len_addr);
    printf("  len_data = %d,\n", config.len_data);
    printf("  len_wen = %d\n", config.len_wen);
    printf("}\n");
}

static PyObject* gen_prog_waveform(PyObject* self, PyObject* args) {
    const int wen_width = 1;
    PyArrayObject* prog_data = NULL;
    struct jtag_config config;
    int trstn_polarity;

    // Parse arguments
    if (!PyArg_ParseTuple(args, "O!iiiiiiii",
            &PyArray_Type, &prog_data,
            &config.len_ir,
            &config.ir_addr,
            &config.ir_data,
            &config.ir_wen,
            &config.len_addr,
            &config.len_data,
            &config.len_wen,
            &trstn_polarity
    )) {
        return NULL;
    }
    // print_config(config);

    // deassert TRSTn based on polarity
    TRSTn = (trstn_polarity == 1) ? 0 : 1;

    /* validate input array dimensions */
    if (PyArray_NDIM(prog_data) != 2) {
        PyErr_SetString(PyExc_ValueError, "`prog_data` must be two dimensional");
        return NULL;
    }
    if (PyArray_DIM(prog_data, 1) != 2) {
        PyErr_SetString(PyExc_ValueError, "`prog_data.shape[1]` must be equal to 2");
        return NULL;
    }
    if (PyArray_TYPE(prog_data) != NPY_UINT) {
        PyErr_SetString(PyExc_ValueError, "`prog_data` must have type `np.uint32`");
        return NULL;
    }

    /* Pre-allocate new numpy array */
    npy_intp dims[2] = {-1, 4};
    int n_bytes = PyArray_DIM(prog_data,0);
    dims[0] = count_jtag_steps(n_bytes, config);
    PyObject* arr = PyArray_SimpleNew(2, dims, NPY_UBYTE);
    if (arr == NULL) {
        PyErr_SetString(PyExc_RuntimeError, "could not allocate output array");
        return NULL;
    }

    /* Populate numpy array */
    populate_array(prog_data, arr, config);

    return arr;
}

static PyMethodDef my_methods[] = {
    {"gen_prog_waveform", gen_prog_waveform, METH_VARARGS, "Sample text."},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef jtag_programmer_kernel = {
    PyModuleDef_HEAD_INIT,
    "jtag_programmer_kernel",
    "Compute kernel for accelerating A-Core JTAG programming waveform generation.",
    -1,
    my_methods,
};

PyMODINIT_FUNC PyInit_jtag_programmer_kernel(void) {
    import_array();
    return PyModule_Create(&jtag_programmer_kernel);
}
